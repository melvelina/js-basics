console.log("decisions tasks:");

// TASK 1: rewrite with ternary
function printResult(a, b) {
  a + b < 4 ? console.log("Below") : console.log("Over");
}

printResult(1, 2); //Bellow
printResult(2, 2); //Over
printResult(2, 3); //Over

// TASK 2:
// Write an “if” condition to check that age is between 14 and 90 inclusively
// log to console 'in range' or 'out of range'

const age = 90; // sample value

if (age <= 90 && age >= 14) {
  console.log("in range");
} else {
  console.log("out of range");
}

// TASK3: rewrite to a switch
const a = 4;

switch (a) {
  case 0:
    console.log("Nothing found");
    break;

  case 1:
    console.log("1 item found");
    break;

  case 2:
  case 3:
    console.log("Several items found");
    break;

  default:
    console.log("Unexpected input");
}


// alternative varinat
const a = 4;

switch (true) {
  case (a == 0):
    console.log("Nothing found");
    break;

  case (a == 1):
    console.log("1 item found");
    break;

  case (a == 2 || a == 3):
    // case 3:
    console.log("Several items found");
    break;

  default:
    console.log("Unexpected input");
}