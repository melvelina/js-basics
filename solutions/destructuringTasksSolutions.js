console.log("destructuring tasks:");

// TASK 1:
//destructure both title properties and console.log it
const metadata = {
  title: "JavaScript",
  translations: [
    {
      locale: "en",
      title: "basics",
    },
  ],
};

let {
  title: javaScriptTitle, // rename
  translations: [
    {
      title: localeTitle, // rename
    },
  ],
} = metadata;

console.log(javaScriptTitle); // "JavaScript"
console.log(localeTitle); // "Basics"

//TASK 2:
//destructure third element name property ('Daniel') and console it

const persons = [
  { id: 1, name: "John" },
  { id: 2, name: "Mary" },
  { id: 3, name: "Daniel" },
];

const [, , { name }] = persons;

console.log(name); // "Daniel"
