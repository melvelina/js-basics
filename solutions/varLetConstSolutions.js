console.log("var let const tasks:");

//Task1 - can not do not assign value to const -> use let
let var1;
console.log("Task 1 completed");

//Task 2 '-' not allowed, can't start with digit
let my_name = "My name";
let forYou = "for you";
console.log("Task 2 completed");

//Task 3 - can not use reserved words: array, var, function and etc.
const newArray = [1, 2, 3];
const variable = "var";
console.log("Task 3 completed");

//Task 4 - const can not be reassigned, use let
let obj = {
  prop: "prop",
  prop2: "prop2",
};

obj = {
  newProp: "newProp",
};

console.log(obj); // { newProp: 'newProp' }
console.log("Task 4 completed");

// Task 5 - let do not allow redeclare. remove let from second line
let points = 50;
points = 60;
console.log(points); //60

//Task 6 - var is functional scope, so use let, which is block scope and declare let outside if statement
let city;
if (!city) {
  let city = "Vilnius";
  console.log(city); //Vilnius
}
console.log(city); // undefined
console.log("Task 6 completed");