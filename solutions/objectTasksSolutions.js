console.log("object tasks:");

// TASK 1:
// log to console 'prop1' and 'new prop' values
const obj1 = {
  prop1: "prop1",
  "new prop": "new",
  prop2: "prop2",
};

// write your code here...
console.log(obj1.prop1, obj1["new prop"]);

//=============================//
// TASK 2:
// log to console obj1 keys and values

console.log(Object.keys(obj1));
console.log(Object.values(obj1));

//=============================//

// TASK3: delete prop2 propertie from obj1

// write TASK 3 code here...
delete obj1.prop2;
console.log(obj1.prop2); //undefined
