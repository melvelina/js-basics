console.log("arrow functions tasks:");

// TASK 1:
const sum = (num1, num2) => num1 + num2;

// TASK 2
const stringLength = (str) => {
  let length = str.length;
  console.log(`the length of "${str}" is:`, length);
  return str.length;
};

stringLength("willynilly");

//TASK 3:
//create arrow function which console name and age
// assign default values to both arguments
// try case when pass no values, pass one value, pass both values

const nameAndAge = (name = "John", age = 30) =>
  console.log("Name: " + name + " , age: " + age);

nameAndAge();
nameAndAge("Daniel");
nameAndAge("Maja", 24);
