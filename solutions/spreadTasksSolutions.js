console.log("spread and rest tasks:");

// TASK 1: concatenate two arrays with spread operator
let numbers = [1, 2];
let moreNumbers = [3, 4];
let allNumbers = [...numbers, ...moreNumbers];
console.log(allNumbers); // [1, 2, 3, 4]

// TASK 2: use spread operator and add discountType property in discount
//         copy old information as well

const formValues = {
  termName: "Term name",
  id: 1,
  discount: {
    discountName: "Discount name",
    discountValue: "5%",
  },
};

const updatedValues = {
  ...formValues,
  discount: {
    ...formValues.discount,
    discountType: "Revenue",
  },
};

console.log(updatedValues);

// TASK 3: take last three properties from object with rest operator
//         console it
const styles = {
  margin: "10px",
  padding: 0,
  color: "red",
  border: "1px solid grey",
  background: "tranparent",
};

const { margin, padding, ...rest } = styles;

console.log(rest);
