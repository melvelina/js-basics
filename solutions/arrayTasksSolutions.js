console.log("array tasks:");

// TASK 1:
// filter cities which population is equal or bigger than 3000000

let cities = [{
    name: "Los Angeles",
    population: 3792621
  },
  {
    name: "New York",
    population: 8175133
  },
  {
    name: "Chicago",
    population: 2695598
  },
  {
    name: "Houston",
    population: 2099451
  },
  {
    name: "Philadelphia",
    population: 1526006
  },
];

let bigCities = cities.filter((city) => city.population >= 3000000);

console.log(bigCities);

/* ecpected output: 
[
  { name: 'Los Angeles', population: 3792621 },
  { name: 'New York', population: 8175133 }
]
*/

// TASK 2: filter cities which population are less than 3000000
//         chain map() method to console.log city name and population in same line

cities
  .filter((city) => city.population < 3000000)
  .map((city) => console.log(city.name + ":" + city.population));

//TASK 3: use reduce() method and output the population sum of all cities

const allPopulation = cities.reduce((sum, city) => {
  return sum + city.population;
}, 0);

console.log(allPopulation);