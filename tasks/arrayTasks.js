console.log("array tasks:");

// TASK 1:
// filter cities which population equal or bigger than 3000000

let cities = [
  { name: "Los Angeles", population: 3792621 },
  { name: "New York", population: 8175133 },
  { name: "Chicago", population: 2695598 },
  { name: "Houston", population: 2099451 },
  { name: "Philadelphia", population: 1526006 },
];

// your code here

/* ecpected output: 
[
  { name: 'Los Angeles', population: 3792621 },
  { name: 'New York', population: 8175133 }
]
*/

/*TASK 2: filter cities which population are less than 3000000
//         chain map() method to console.log city name and population in same line

// Hint obj.filter(....).map(....) */

//TASK 3: use reduce() method and output the population sum of all cities
