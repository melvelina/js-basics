console.log("decisions tasks:");

// TASK 1: rewrite with ternary
function printResult(a, b) {
  if (a + b < 4) {
    console.log("Below");
  } else {
    console.log("Over");
  }
}

printResult(1, 2); //Bellow
printResult(2, 2); //Over
printResult(2, 3); //Over

// TASK 2:
// Write an “if” condition to check that age is between 14 and 90 inclusively
// log to console 'in range' or 'out of range'

const age = 20; // sample value

// write your if here...

// TASK3: rewrite to a switch
const a = 4;

if (a === 0) {
  console.log("Nothing found");
} else if (a === 1) {
  console.log("1 item found");
} else if (a === 2 || a === 3) {
  console.log("Several items found");
} else {
  console.log("Unexpected input");
}
