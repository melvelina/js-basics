console.log('var let const tasks:');

//Task1 - fix error
const var1;
console.log('Task 1 completed');



//Task 2 - fix naming errors
let my-name = 'My name';
let 4you = 'for you';
console.log('Task 2 completed');



//Task 3 - fix naming errors
const array = [1, 2, 3];
const var = 'var';
console.log('Task 3 completed');



//Task 4 - fix error and print '{ newProp: 'newProp' }' to console
const obj = {
    prop: 'prop',
    prop2: 'prop2',
};

obj = {
    newProp: 'newProp'
}

console.log(obj); // { newProp: 'newProp' }
console.log('Task 4 completed');



// Task 5 - fix error and print '60' in console
let points = 50;
let points = 60;
console.log(points); //60
console.log('Task 5 completed');



//Task 6 - print 'Vilnius' in first console and 'undefined' in second
if(!city) {
    var city = "Vilnius";
    console.log(city); //Vilnius
}
//do not change line below
console.log(city); // undefined

console.log("Task 6 completed");