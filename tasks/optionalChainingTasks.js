console.log("optional chaining:");

// TASK 1: rewrite if statement with optional chaining
//         use tempalte litterals for output
const formValues = {
  termName: "Term name",
  id: 1,
  discount: {
    discountName: "Discount name",
    discountValue: "5%",
    details: {
      startDate: "2020-10-21",
      endDate: "2022-02-22",
    },
  },
};

//change code below
if (formValues && formValues.discount && formValues.discount.details) {
  console.log(
    "Start date: " +
      formValues.discount.details.startDate +
      " End date: " +
      formValues.discount.details.endDate
  );
}
