console.log("arrow functions tasks:");

// TASK 1:
// convert to arrow function
function sum(num1, num2) {
  return num1 + num2;
}

console.log(sum(10, -5)); //5
console.log(sum(40, 2)); //42

// TASK 2
// convert to arrow function
function stringLength(str) {
  let length = str.length;
  console.log(`the length of "${str}" is:`, length);
  return str.length;
}

stringLength("willynilly");

//TASK 3:
//create arrow function which console name and age
// assign default values to both arguments
// try case when pass no values, pass one value, pass both values

//your code HERE
